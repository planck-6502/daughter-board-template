EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x25_Odd_Even J1
U 1 1 5FD62DCD
P 2500 3450
F 0 "J1" H 2550 4867 50  0000 C CNN
F 1 "Conn_01x40" H 2550 4776 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x25_P2.54mm_Horizontal" H 2500 3450 50  0001 C CNN
F 3 "~" H 2500 3450 50  0001 C CNN
	1    2500 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5FD64472
P 3000 3350
F 0 "#PWR0101" H 3000 3200 50  0001 C CNN
F 1 "+5V" H 3015 3523 50  0000 C CNN
F 2 "" H 3000 3350 50  0001 C CNN
F 3 "" H 3000 3350 50  0001 C CNN
	1    3000 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3350 2800 3350
$Comp
L power:+5V #PWR0102
U 1 1 5FD65A34
P 2000 3450
F 0 "#PWR0102" H 2000 3300 50  0001 C CNN
F 1 "+5V" H 2015 3623 50  0000 C CNN
F 2 "" H 2000 3450 50  0001 C CNN
F 3 "" H 2000 3450 50  0001 C CNN
	1    2000 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3450 2050 3450
$Comp
L power:GND #PWR0103
U 1 1 5FD65E00
P 3000 3450
F 0 "#PWR0103" H 3000 3200 50  0001 C CNN
F 1 "GND" H 3005 3277 50  0000 C CNN
F 2 "" H 3000 3450 50  0001 C CNN
F 3 "" H 3000 3450 50  0001 C CNN
	1    3000 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3450 2800 3450
$Comp
L power:GND #PWR0104
U 1 1 5FD66D1A
P 1850 3300
F 0 "#PWR0104" H 1850 3050 50  0001 C CNN
F 1 "GND" H 1855 3127 50  0000 C CNN
F 2 "" H 1850 3300 50  0001 C CNN
F 3 "" H 1850 3300 50  0001 C CNN
	1    1850 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3350 2100 3300
Wire Wire Line
	2100 3300 1850 3300
Wire Wire Line
	2100 3350 2300 3350
Text GLabel 2300 2250 0    50   Input ~ 0
A0
Text GLabel 2300 2350 0    50   Input ~ 0
A1
Text GLabel 2300 2450 0    50   Input ~ 0
A2
Text GLabel 2300 2550 0    50   Input ~ 0
A3
Text GLabel 2300 2650 0    50   Input ~ 0
A4
Text GLabel 2300 2750 0    50   Input ~ 0
A5
Text GLabel 2300 2850 0    50   Input ~ 0
A6
Text GLabel 2300 2950 0    50   Input ~ 0
A7
Text GLabel 2300 3050 0    50   Input ~ 0
A8
Text GLabel 2300 3150 0    50   Input ~ 0
A9
Text GLabel 2300 3250 0    50   Input ~ 0
A10
Text GLabel 2300 3550 0    50   Input ~ 0
A11
Text GLabel 2300 3650 0    50   Input ~ 0
A12
Text GLabel 2300 3750 0    50   Input ~ 0
A13
Text GLabel 2300 3850 0    50   Input ~ 0
A14
Text GLabel 2300 3950 0    50   Input ~ 0
A15
Text GLabel 2800 2250 2    50   BiDi ~ 0
D0
Text GLabel 2800 2350 2    50   BiDi ~ 0
D1
Text GLabel 2800 2450 2    50   BiDi ~ 0
D2
Text GLabel 2800 2550 2    50   BiDi ~ 0
D3
Text GLabel 2800 2650 2    50   BiDi ~ 0
D4
Text GLabel 2800 2750 2    50   BiDi ~ 0
D5
Text GLabel 2800 2850 2    50   BiDi ~ 0
D6
Text GLabel 2800 2950 2    50   BiDi ~ 0
D7
Text GLabel 2300 4650 0    50   Input ~ 0
~RESET~
Text GLabel 2800 4650 2    50   BiDi ~ 0
~NMI~
Text GLabel 1750 4050 0    50   Input ~ 0
RDY
Text GLabel 1750 4150 0    50   Input ~ 0
BE
Text GLabel 2300 4250 0    50   Input ~ 0
CLK
Text GLabel 2300 4350 0    50   Input ~ 0
R~W~
Text GLabel 2300 4550 0    50   Input ~ 0
SYNC
Text GLabel 1750 4450 0    50   BiDi ~ 0
~IRQ~
Text GLabel 2800 4550 2    50   BiDi ~ 0
~IRQ0~
Text GLabel 2800 4450 2    50   BiDi ~ 0
~IRQ1~
Text GLabel 2800 4350 2    50   BiDi ~ 0
~IRQ2~
Text GLabel 2800 4250 2    50   BiDi ~ 0
~IRQ3~
$Comp
L Device:C_Small C2
U 1 1 5FD8A65F
P 3150 3400
F 0 "C2" H 3242 3446 50  0000 L CNN
F 1 "C_Small" H 3242 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3150 3400 50  0001 C CNN
F 3 "~" H 3150 3400 50  0001 C CNN
	1    3150 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3300 3100 3300
Wire Wire Line
	3100 3300 3100 3350
Wire Wire Line
	3100 3350 3000 3350
Connection ~ 3000 3350
Wire Wire Line
	3150 3500 3100 3500
Wire Wire Line
	3100 3500 3100 3450
Wire Wire Line
	3100 3450 3000 3450
Connection ~ 3000 3450
$Comp
L Device:C_Small C1
U 1 1 5FD8C99F
P 1650 3400
F 0 "C1" H 1742 3446 50  0000 L CNN
F 1 "C_Small" H 1742 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1650 3400 50  0001 C CNN
F 3 "~" H 1650 3400 50  0001 C CNN
	1    1650 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3300 1850 3300
Connection ~ 1850 3300
Wire Wire Line
	1650 3500 2050 3500
Wire Wire Line
	2050 3500 2050 3450
Connection ~ 2050 3450
Wire Wire Line
	2050 3450 2000 3450
Wire Wire Line
	1750 4450 2300 4450
Wire Wire Line
	1750 4050 2300 4050
Wire Wire Line
	1750 4150 2300 4150
Text GLabel 2800 3550 2    50   BiDi ~ 0
~SSEL~
Text GLabel 2800 3650 2    50   BiDi ~ 0
~INH~
Text GLabel 2800 3750 2    50   BiDi ~ 0
~SLOT_SEL~
Text GLabel 2800 3850 2    50   BiDi ~ 0
LED1
Text GLabel 2800 3950 2    50   BiDi ~ 0
LED2
Text GLabel 2800 4050 2    50   BiDi ~ 0
LED3
Text GLabel 2800 4150 2    50   BiDi ~ 0
LED4
Text GLabel 2800 3050 2    50   BiDi ~ 0
EX0
Text GLabel 2800 3150 2    50   BiDi ~ 0
EX1
Text GLabel 2800 3250 2    50   BiDi ~ 0
EX2
$EndSCHEMATC
